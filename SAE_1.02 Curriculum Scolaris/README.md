Nom du jeu : Curriculum Scolaris

Objectif: 
Battez-vous contre des élèves en répondant à leurs diverses questions. Fouillez tous les recoins de l'établissement pour les trouver ainsi que le coffre contenant la clé vous permettant d'accéder à la bibliothèque où vous pourrez trouver les derniers élèves.

Description:
Vous êtes un étudiant dans une. Les élèves sont là pour vous barrez la route et vous devez les battre pour pouvoir sortir. Vous devez également trouver un coffre pour vous aider dans votre quête.

Caractéristiques:
  Un gameplay unique qui combine l'aventure et le quiz.
  Des graphismes en ASCII.
  Une map interactive.

Fin du jeu:
Vous gagnez le jeu si votre intelligence est de 16 ou plus.
Vous avez fini le jeu si vous avez 21 d’intelligence.
(1 point d’intelligence par élève battu + 1 pour avoir résolu le coffre.)

Légende:

    “ P “: Personnage non-joueur
		En bleu si il a déjà été battu.
		En rouge si il n’a pas été battu.
    "[+]":Coffre : Contient la clé de la bibliothèque.
    "| |" : Porte vers une autre salle.
    "|&|" : Porte verrouillé 
    " Ʒ "  : Joueur

Instructions:

    Pour lancer le jeu ouvrez un terminal (ouverture du terminale crtl+alt+t) et déplacez vous dans le dossier CurriculumScolaris puis exécutez  compile.sh puis run.sh 
Vous pouvez vous déplacer à l’aide des touches :
           Z
    Q      S      D
Pour battre un PNJ , vous devez répondre correctement à toutes ses questions en une seule fois.
Un coffre est dans l'école, la clé de la bibliothèque s'obtient en résolvant le puzzle du coffre.    
il vous suffit d’entrer la réponse puis d’appuyer sur entrer pour valider vos réponses aux questions.
	Il y a des questions où il faut recopier la bonne réponse,
	d’autre où il faut juste entrer la lettre correspondant à la bonne réponse
et finalement des questions sans choix possibles ou il faut entrer la bonne réponse.

Crédits:

    Développeur : [Arthur Goddefroy]
                  [Augustin Delfly ]

