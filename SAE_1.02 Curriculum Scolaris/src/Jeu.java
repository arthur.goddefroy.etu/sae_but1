import extensions.CSVFile;
import extensions.File;

/* pour version finale.
class Parametre{
   boolean interface=true;
}
*/

class Jeu extends Program{

    // attribution d'un symboles reconnaissable aux different elements du jeu
    final String PORTE          = "| |" ;
    final String PORTE_FERMEE   = "|&|" ;
    final String MUR_HORIZONTAL = "___" ;
    final String MUR_VERTICAL   =  "|"  ;
    final String VIDE           = "   " ;
    final String COFFRE         = "[+]" ;
    final String PNJ            = " P " ;
    final String[][] JOUEUR_SKIN=new String[][] {{"Ʒ  "," Ʒ ","  Ʒ"},{"Ƹ  "," Ƹ ","  Ƹ"}};
    final String SORTIE         =  "N"  ;
    final String BIBLIO         = "|B|" ;

    // attribution de valeur à des variable pour mieux reconnaitre l'utilité des valeurs.
    final int DROITE = 0;
    final int GAUCHE = 1;

    final int DEPLACEMENT=0;
    final int INTERACTION=1;
    final int MENU=2;

    final int CLE=0;
    final int TROPHEE=1;

    boolean[] state= new boolean[]{true,false,false};
    // création des pnjs
    Pnj[] pnjs=initPnjs();
    // création des 5 différentes zones
    final int hall=0;
    final int français=1;
    final int bibliotheque=2;
    final int mathématiques=3;
    final int histoireGeographie=4;

    boolean GAME=true;

    void algorithm(){
        clearTheScreen();

        Zone[] listeZone=new Zone[5];
        listeZone[0]=initHall(0);
        listeZone[1]=initSalleDeClasse("Salle de français",19,5,20,1);
        listeZone[2]=initBibliotheque(19,24,25,2);
        listeZone[3]=initSalleDeClasse("Salle de mathématiques",0,5,20,3);
        listeZone[4]=initSalleDeClasse("Salle d'histoire-géographie",0,5,20,4);
        
        // création du personnage
        Joueur joueur=initJoueur(listeZone); 
        //ligne deplacé dans la fonction charger(), ligne 215  
        placerPnjs(listeZone);
        clearTheScreen();
        charger(joueur,listeZone);

        println(joueur.zoneCourante.nomDeZone);
        afficherZone(joueur.zoneCourante);
        
        while(GAME){ 
            clearTheScreen();
            if(state[DEPLACEMENT]){
                actualiserMaps(joueur);
                afficherZone(joueur.zoneCourante);
                action(joueur, listeZone);
            } 
            if(state[INTERACTION]){
                clearTheScreen();
                interragir(joueur,listeZone);
                readString();
            }
            if(state[MENU]){
                clearTheScreen();
                menu(joueur);

            }
        }
        if(joueur.inteligence>=16){
            println("Bravo!");
        }
    }

    boolean stringToBool(String vf){
        if (vf=="0"){// true remplacé par 0
            return true;
        }else{
            return false;
        }
    }

    void drawASCII(String path){
        File f2 = newFile(path);
        while(ready(f2)){
            println(readLine(f2));
        }
    }

    void clearTheScreen(){
        for(int i=0; i<30;i=i+1){
            print("\n");
        }
    }

    void lore(){
        for(int bulle=1;bulle<6;bulle=bulle+1){
            drawASCII("ressources/draw/lore"+bulle+".txt");
            readString();
        }
    }

    void menu(Joueur joueur){
        println("Entrez le numero correspondant à votre choix :");
        println("1- Sauvegarder");
        println("2- "+joueur.nom);
        println("3- Quitter le jeu");
        println("4- Retour");
        String option="";
        do{
            option=readString();
        }while(!equals(option,"1") && !equals(option,"2") && !equals(option,"3") && !equals(option,"4"));
        clearTheScreen();
        if(equals(option,"1")){
            save(joueur);
        }else if(equals(option,"2")){
            println(joueur.nom+"\n inteligence :"+joueur.inteligence);
            println("posséde :");
            if(joueur.inventaire[CLE]){println("Clé de la bibliothéque");}  // ajout du point virgule
            if(joueur.inventaire[TROPHEE]){println("Trophée ");}            // ajout du point virgule
            println("\n\n\n\n\n Appuyez sur entrée.");                      // ajout du point virgule
            readString();
        }else if(equals(option,"3")){
            GAME=false;
        }
        else{
            println("retour.");
        }
        state[MENU]=false;
        state[DEPLACEMENT]=true;
    }

    void save(Joueur joueur){
        CSVFile sauvegardes = loadCSV("ressources/CSV/saves.csv");
        int lignes = rowCount(sauvegardes);
        int colonnes = columnCount(sauvegardes);
        String[][] data=new String[lignes][colonnes];
        int l=0;
        while(l<lignes && !equals(getCell(sauvegardes,l,0),joueur.nom)){
            for(int c=0;c<colonnes;c=c+1){
                data[l][c]=getCell(sauvegardes,l,c);
                l=l+1;
            }
        }
        if(equals(getCell(sauvegardes,l,0),joueur.nom)){
            data[l][0]=joueur.nom;
            data[l][1]=""+joueur.zoneCourante.id;
            data[l][2]=""+joueur.positionX;
            data[l][3]=""+joueur.positionY;
            data[l][4]=""+joueur.inventaire[0];
            data[l][5]=""+joueur.inventaire[1];
            data[l][6]=""+joueur.inteligence;
            for(int perso=0;perso<length(pnjs);perso=perso+1){
                data[l][7+perso]=""+pnjs[perso].vaincu;
            }
            l=l+1;
            while(l<lignes ){
                for(int c=0;c<colonnes;c=c+1){
                    data[l][c]=getCell(sauvegardes,l,c);
                    l=l+1;
                }
            }
        }else{
            data[l][0]=joueur.nom;
            data[l][1]=""+joueur.zoneCourante.id;
            data[l][2]=""+joueur.positionX;
            data[l][3]=""+joueur.positionY;
            data[l][4]=""+joueur.inventaire[0];
            data[l][5]=""+joueur.inventaire[1];
            data[l][6]=""+joueur.inteligence;
            for(int perso=1;perso<length(pnjs);perso=perso+1){
                data[l][6+perso]=""+pnjs[perso].vaincu;
            }
        }
        saveCSV(data,"ressources/CSV/saves.csv");
    }

    void charger(Joueur joueur,Zone[] listeZone){
        println("Comment t'appelles-tu?");
        String nom=readString();
        CSVFile sauvegardes = loadCSV("ressources/CSV/saves.csv");
        int lignes = rowCount(sauvegardes);
        int colonnes = columnCount(sauvegardes);
        int l=0;
        while(l<lignes-1 && !equals(getCell(sauvegardes,l,0),nom)){
            l=l+1;
        }
        if(equals(getCell(sauvegardes,l,0),nom)){
            joueur.nom=getCell(sauvegardes,l,0);
            joueur.zoneCourante=listeZone[stringToInt(getCell(sauvegardes,l,1))];
            joueur.positionX=stringToInt(getCell(sauvegardes,l,2));
            joueur.positionY=stringToInt(getCell(sauvegardes,l,3));
            joueur.inventaire[0]=stringToBool(getCell(sauvegardes,l,4));
            joueur.inventaire[1]=stringToBool(getCell(sauvegardes,l,5));
            joueur.inteligence=stringToInt(getCell(sauvegardes,l,6));
            for(int perso=1;perso<length(pnjs);perso=perso+1){
                pnjs[perso].vaincu=stringToBool(getCell(sauvegardes,l,6+perso));
            }
        }else{
            listeZone[hall].terrain[joueur.positionX][joueur.positionY]=JOUEUR_SKIN[joueur.direction][joueur.etat];
            lore();
        }
    }

    void actualiserMaps(Joueur joueur){
        joueur.zoneCourante.positionXjoueur=joueur.positionX;
        joueur.zoneCourante.positionYjoueur=joueur.positionY;
        joueur.zoneCourante.terrain[joueur.positionX][joueur.positionY]=JOUEUR_SKIN[joueur.direction][joueur.etat];
    }

    void action(Joueur joueur,Zone[]listeZone){
        println("quelle est votre action : \n z : haut \n s : bas \n q : gauche  \n d : droite \n e : interragir \n m : menu ");
        String commande=readString();
        if(!equals(commande,"")){
            char action=charAt(commande,0);
            if(action=='z'||action=='q'||action=='s'||action=='d' ){
                deplacement(action,joueur, listeZone);
                joueur.vise=action;
            }
            if(action=='e'){
                state[DEPLACEMENT]=false;
                state[INTERACTION]=true;
            }
            if(action=='m'){
                state[DEPLACEMENT]=false;
                state[MENU]=true;
            }
        }
    }

    void interragir(Joueur joueur, Zone[] listeZone){
        if(joueur.vise=='d'){
            if(length(joueur.zoneCourante.terrain[joueur.positionX+1][joueur.positionY])>3){
                if(equals(substring(joueur.zoneCourante.terrain[joueur.positionX+1][joueur.positionY],0,3),"pnj")){
                    String perso=substring(joueur.zoneCourante.terrain[joueur.positionX+1][joueur.positionY],3,5);
                    int nb=stringToInt(perso);
                    if(pnjs[nb].vaincu==false){
                        poserQuestion(pnjs[nb],joueur);
                    }
                }
            }
            if (equals(joueur.zoneCourante.terrain[joueur.positionX+1][joueur.positionY],VIDE)){
                println("Rien... essaie peut-être devant un élève?");
            }
            if(equals(joueur.zoneCourante.terrain[joueur.positionX+1][joueur.positionY],COFFRE)){
                if(!joueur.inventaire[CLE]){
                    relier(joueur);
                }
            }   
        }
        if(joueur.vise=='q'){
            if(length(joueur.zoneCourante.terrain[joueur.positionX-1][joueur.positionY])>3){
                if(equals(substring(joueur.zoneCourante.terrain[joueur.positionX-1][joueur.positionY],0,3),"pnj")){
                    String perso=substring(joueur.zoneCourante.terrain[joueur.positionX-1][joueur.positionY],3,5);
                    int nb=stringToInt(perso);
                    if(pnjs[nb].vaincu==false){ // rajout de la condition if.
                        poserQuestion(pnjs[nb],joueur);
                    }
                }
            }
            if(equals(joueur.zoneCourante.terrain[joueur.positionX-1][joueur.positionY],"N")){
                if(joueur.inteligence>=16){
                    GAME=false; // inversion de cette ligne avec la ligne 276
                }else{
                    println("sortie bloquée");
                }
                
            }
            if(equals(joueur.zoneCourante.terrain[joueur.positionX-1][joueur.positionY],COFFRE)){
                if(!joueur.inventaire[CLE]){
                    relier(joueur);
                }
            }else if (equals(joueur.zoneCourante.terrain[joueur.positionX-1][joueur.positionY],VIDE)){
                println("Rien... essaie peut-être devant un élève?");
            }  
        }
        if(joueur.vise=='z'){
            if(length(joueur.zoneCourante.terrain[joueur.positionX][joueur.positionY-1])>3){
                if(equals(substring(joueur.zoneCourante.terrain[joueur.positionX][joueur.positionY-1],0,3),"pnj")){
                    String perso=substring(joueur.zoneCourante.terrain[joueur.positionX][joueur.positionY-1],3,5);
                    int nb=stringToInt(perso);
                    if(pnjs[nb].vaincu==false){
                        poserQuestion(pnjs[nb],joueur);
                    }
                }
            }
            if(equals(joueur.zoneCourante.terrain[joueur.positionX][joueur.positionY-1],COFFRE)){
                if(!joueur.inventaire[CLE]){
                    relier(joueur);
                }
            }
            else if (equals(joueur.zoneCourante.terrain[joueur.positionX][joueur.positionY-1],VIDE)){
                println("Rien... essaie peut-être devant un élève?");
            }
            else if(equals(joueur.zoneCourante.terrain[joueur.positionX][joueur.positionY-1],PORTE_FERMEE)){
                println("La porte semble fermée à clé.");
                if(joueur.inventaire[CLE]==true){
                    println("Vous utilisez la clé pour ouvrir la porte");
                    listeZone[hall].terrain[30][0]=PORTE;
                }
                
            }
        }
        if(joueur.vise=='s'){
            if(length(joueur.zoneCourante.terrain[joueur.positionX][joueur.positionY+1])>3){
                if(equals(substring(joueur.zoneCourante.terrain[joueur.positionX][joueur.positionY+1],0,3),"pnj")){
                    String perso=substring(joueur.zoneCourante.terrain[joueur.positionX][joueur.positionY+1],3,5);
                    int nb=stringToInt(perso);
                    if(pnjs[nb].vaincu==false){
                        poserQuestion(pnjs[nb],joueur);
                    }
                }
            }
            if(equals(joueur.zoneCourante.terrain[joueur.positionX][joueur.positionY+1],COFFRE)){
                if(!joueur.inventaire[CLE]){
                    relier(joueur);
                }
            }
            else if (equals(joueur.zoneCourante.terrain[joueur.positionX][joueur.positionY+1],VIDE)){
                println("Rien... essaie peut-être devant un élève?");
            }
        }
        state[DEPLACEMENT]=true;
        state[INTERACTION]=false;
    }
 
    void deplacement(char direction, Joueur joueur, Zone[] listeZone){
        if(direction=='z' && (joueur.zoneCourante.terrain[joueur.positionX][joueur.positionY-1]=="   " || equals(joueur.zoneCourante.terrain[joueur.positionX][joueur.positionY-1],PORTE))){
            if(equals(joueur.zoneCourante.terrain[joueur.positionX][joueur.positionY-1],PORTE)){
                deplacementVersZone(joueur,listeZone);
            }else{
                joueur.positionY=joueur.positionY-1;
            }
        }else if(direction=='s' && (joueur.zoneCourante.terrain[joueur.positionX][joueur.positionY+1]=="   " || equals(joueur.zoneCourante.terrain[joueur.positionX][joueur.positionY+1],PORTE))){
            if(equals(joueur.zoneCourante.terrain[joueur.positionX][joueur.positionY+1],PORTE)){
                deplacementVersZone(joueur,listeZone);
            }else{
                joueur.positionY=joueur.positionY+1;
            }
        }else if(direction=='q' && joueur.zoneCourante.terrain[joueur.positionX-1][joueur.positionY]=="   "){
            if(joueur.etat==1){
                if(joueur.direction==DROITE){
                    joueur.etat=0;
                }
                joueur.direction=DROITE;
            }else if (joueur.etat==2){
                if(joueur.direction==DROITE){
                    joueur.etat=1;
                }
                joueur.direction=DROITE;
            } else if(joueur.etat==0){
                if(joueur.direction==DROITE){
                    joueur.positionX=joueur.positionX-1;
                    joueur.etat=2;
                }
                joueur.direction=DROITE;
            }
        }else if(direction=='d' && joueur.zoneCourante.terrain[joueur.positionX+1][joueur.positionY]=="   "){
            if(joueur.etat==1){
                if(joueur.direction==GAUCHE){
                    joueur.etat=2;
                }
                    joueur.direction=GAUCHE;
            }else if (joueur.etat==2){
                if(joueur.direction==GAUCHE){
                    joueur.etat=0;
                    joueur.positionX=joueur.positionX+1;
                }
                joueur.direction=GAUCHE;          
            }else{
                if(joueur.direction==GAUCHE){
                    joueur.etat=1;
                }
                joueur.direction=GAUCHE;
            }
        }
        joueur.zoneCourante.terrain[joueur.zoneCourante.positionXjoueur][joueur.zoneCourante.positionYjoueur]="   ";
        joueur.zoneCourante.positionXjoueur=joueur.positionX;
        joueur.zoneCourante.positionYjoueur=joueur.positionY;
        joueur.zoneCourante.terrain[joueur.positionX][joueur.positionY]=JOUEUR_SKIN[joueur.direction][joueur.etat];
    }

    void deplacementVersZone(Joueur joueur, Zone[] listeZone){
        Zone newZone=listeZone[hall];    
        if(joueur.zoneCourante.nomDeZone=="Hall"){        
            listeZone[hall]=joueur.zoneCourante;
            if (joueur.positionX==6){
                if(joueur.positionY==1){
                    println("français");
                    newZone=listeZone[français];
                }else if(joueur.positionY==7){
                    println("histoire géo");
                    newZone=listeZone[histoireGeographie];
                }
            }else{
                if(joueur.positionY==1 && equals(joueur.zoneCourante.terrain[30][8],PORTE)){
                    println("Bibliothèque");
                    newZone=listeZone[bibliotheque];
                }else if(joueur.positionY==7){
                    println("mathématiques");
                    newZone=listeZone[mathématiques];
                }
            }

        }else{
            newZone=listeZone[hall];
        }
        joueur.zoneCourante=newZone;
        joueur.positionX=joueur.zoneCourante.positionXjoueur;
        joueur.positionY=joueur.zoneCourante.positionYjoueur;
    }

    Pnj[] initPnjs(){
        String nom="";
        Pnj[] pnjs=new Pnj [21];
        for(int nb=1; nb<10 ;nb=nb+1){
            Pnj pnj=new Pnj();
            pnj.nomCSV="pnj"+"0"+nb+".csv";
            pnj.vaincu=false;
            pnj.choix=true;
            pnjs[nb]=pnj;
        }
        for(int nb=10; nb<21 ;nb=nb+1){
            Pnj pnj=new Pnj();
            pnj.nomCSV="pnj"+nb+".csv";
            pnj.vaincu=false;
            pnj.choix=true;
            pnjs[nb]=pnj;
        }
        return pnjs;
    }

    void placerPnjs(Zone[] listeZone){
        listeZone[mathématiques].terrain[15][6]=pnjs[1].nomCSV;  pnjs[1].choix=false;
        listeZone[mathématiques].terrain[3][9]=pnjs[2].nomCSV;
        listeZone[mathématiques].terrain[5][18]=pnjs[3].nomCSV;
        listeZone[mathématiques].terrain[16][13]=pnjs[4].nomCSV; pnjs[4].choix=false;

        listeZone[français].terrain[16][5]=pnjs[5].nomCSV;
        listeZone[français].terrain[3][8]=pnjs[6].nomCSV;        pnjs[6].choix=false;
        listeZone[français].terrain[13][18]=pnjs[7].nomCSV;
        listeZone[français].terrain[5][15]=pnjs[8].nomCSV;

        listeZone[histoireGeographie].terrain[17][4]=pnjs[9].nomCSV;
        listeZone[histoireGeographie].terrain[4][7]=pnjs[10].nomCSV;
        listeZone[histoireGeographie].terrain[12][17]=pnjs[11].nomCSV;
        listeZone[histoireGeographie].terrain[6][15]=pnjs[12].nomCSV;

        listeZone[hall].terrain[5][2]=pnjs[13].nomCSV;
        listeZone[hall].terrain[8][7]=pnjs[14].nomCSV;
        listeZone[hall].terrain[13][5]=pnjs[15].nomCSV;
        listeZone[hall].terrain[16][3]=pnjs[16].nomCSV;

        listeZone[bibliotheque].terrain[5][13]=pnjs[17].nomCSV;
        listeZone[bibliotheque].terrain[8][7]=pnjs[18].nomCSV;
        listeZone[bibliotheque].terrain[13][5]=pnjs[19].nomCSV;
        listeZone[bibliotheque].terrain[19][18]=pnjs[20].nomCSV;
    }

    Joueur initJoueur(Zone[] listeZone){
        Joueur joueur=new Joueur();
        joueur.zoneCourante=listeZone[hall];
        joueur.zonePrecedante=listeZone[hall];
        joueur.direction=DROITE;
        joueur.vise='d';
        joueur.etat=0;
        joueur.positionX=1;
        joueur.positionY=5;
        joueur.inventaire=new boolean []{false,false};
        joueur.inteligence=0;
        return joueur;
    }

    Zone newZoneVide(String nom,int tailleX,int tailleY, int id){
        Zone zone=new Zone();
        zone.nomDeZone=nom;
        zone.terrain= new String[tailleX][tailleY];
        zone.id=id;
        return zone;
    }

    Zone initSalleDeClasse(String nomDeLaSalle,int porteY,int porteX,int taille,int id){
        Zone zone=newZoneVide(nomDeLaSalle,taille,taille,id);
        if(porteY==0){
            zone.positionYjoueur=1;
        }else{
            zone.positionYjoueur=porteY-1;
        }
        zone.positionXjoueur=porteX;

        for(int x=0;x<taille;x=x+1){
            for(int y=0;y<taille;y=y+1){
                zone.terrain[x][y]=VIDE;
                if(x==0 || x==taille-1){
                    zone.terrain[x][y]=MUR_VERTICAL;
                }else if(y==0 || y==taille-1){
                    zone.terrain[x][y]=MUR_HORIZONTAL;
                }
                if(y==porteY && x==porteX){
                    zone.terrain[x][y]=PORTE;
                }
            }
        }
        zone.terrain[0][0]=" ";
        zone.terrain[taille-1][0]=" ";
        return zone;
    }

    Zone initBibliotheque(int porteY,int porteX,int taille,int id){
        Zone zone=newZoneVide("Bibliothéque",taille,taille,id);
        for(int x=0;x<taille;x=x+1){
            for(int y=0;y<taille;y=y+1){
                zone.terrain[x][y]=VIDE;
                if(x==0 || x==taille-1){
                    zone.terrain[x][y]=MUR_VERTICAL;
                }else if(y==0 || y==taille-1){
                    zone.terrain[x][y]=MUR_HORIZONTAL;
                }
                if(y==porteY && x==porteX){
                    zone.terrain[y][x]=PORTE;
                }
            }
        }
        zone.terrain[0][0]=" ";
        zone.terrain[taille-1][0]=" ";
        placerBibliothèques(zone);
        return zone;
    }

    void placerBibliothèques(Zone zone){
        for(int x=1;x<8;x=x+1){
            for(int y=3;y<8;y=y+2){
                zone.terrain[x][y]=BIBLIO;
            }
        }
        for(int x=10;x<20;x=x+1){
            for(int y=3;y<8;y=y+2){
                zone.terrain[x][y]=BIBLIO;
            }
        }
    }

    Zone initHall(int id){
        int tailleX=20;
        int tailleY=9;
        int[][] portes=initPortesHall(tailleX,tailleY);
        Zone zone=newZoneVide("Hall",tailleX,tailleY,id);
        for(int x=0;x<tailleX;x=x+1){
            for(int y=0;y<tailleY;y=y+1){
                zone.terrain[x][y]=VIDE;
                if(x==0 || x==tailleX-1){
                    zone.terrain[x][y]=MUR_VERTICAL;
                }else if(y==0 || y==tailleY-1){
                    zone.terrain[x][y]=MUR_HORIZONTAL;
                }
            }
        }

        for(int porte=0;porte<length(portes,1)-1;porte=porte+1){
            zone.terrain[portes[porte][0]][portes[porte][1]]=PORTE;
        }
        zone.terrain[portes[3][0]][portes[3][1]]=PORTE_FERMEE;
        println(portes[3][0]+"  "+portes[3][1]);

        zone.terrain[0][4]=SORTIE;
        zone.terrain[0][5]=SORTIE;
        zone.terrain[0][0]=" ";
        zone.terrain[tailleX-1][0]=" ";
        zone.terrain[tailleX-2][4]=COFFRE;
        return zone;
    }

    int[][] initPortesHall(int tailleX,int tailleY){
        int [][] portes=new int [4][2];
        portes[0][0]= 6             ;portes[0][1]= tailleY-1;
        portes[1][0]= 6             ;portes[1][1]= 0;
        portes[2][0]= tailleX-10    ;portes[2][1]= tailleY-1;
        portes[3][0]= tailleX-10    ;portes[3][1]= 0;
        return portes;
    }

    void afficherZone(Zone zone){
        String string="";
        println("Pièce :");
        println(zone.nomDeZone);
        for(int x=0;x<length(zone.terrain,2);x=x+1){
            for(int y=0;y<length(zone.terrain,1);y=y+1){
                if(length(zone.terrain[y][x])>3){
                    if(equals(substring(zone.terrain[y][x],0,3),"pnj")){
                        String perso=substring(zone.terrain[y][x],3,5);
                        int nb=stringToInt(perso);
                        if(pnjs[nb].vaincu){
                            string=string+ANSI_BLUE+PNJ+ANSI_WHITE;
                        }else{
                            string=string+ANSI_RED+PNJ+ANSI_WHITE;
                        }
                        
                    }
                }else{
                string=string+zone.terrain[y][x];
                }
            }
            string=string+"\n";
        }
        print(string);
    }

    Question newQuestion(String enonce, String q1, String q2, String q3, String q4, String solution){
        Question q = new Question();
        q.quesionPhrase = enonce;
        q.reponse[0] = q1;
        q.reponse[1] = q2;
        q.reponse[2] = q3;
        q.reponse[3] = q4;
        q.solution = solution;
        return q;
    }
    
    void poserQuestion(Pnj pnj,Joueur joueur){
        CSVFile test1 = loadCSV("ressources/CSV/"+pnj.nomCSV);
        int lignes = rowCount(test1);
        int colonnes = columnCount(test1);
        Question[] questions = new Question[colonnes];
        for (int l = 0; l < lignes; l++) {
            questions[l] = newQuestion(
                getCell(test1, l, 0),
                getCell(test1, l, 1),
                getCell(test1, l, 2),
                getCell(test1, l, 3),
                getCell(test1, l, 4),
                getCell(test1, l, 5)
            );  
        }
        boolean perdu=false;
        int q = 0;
        while(q<length(questions)-1 && !perdu){
            drawASCII("ressources/draw/dessin2.txt"); 
            println(questions[q].quesionPhrase);
            if(pnj.choix){
                for (int rep = 0;  rep < length(questions[q].reponse); rep++) {
                    println(questions[q].reponse[rep]);
                }
            }
            String reponse = " "+readString();
            clearTheScreen();
            if(!equals(reponse, questions[q].solution)){
                println("Votre réponse : " + reponse +" est fausse, la solution était "+questions[q].solution);
                perdu=true;
            }else{
                println("bravo");
            }
            q+=1;   
        }
        if(perdu==false){
            joueur.inteligence=joueur.inteligence+1;
            pnj.vaincu=true;
        }else{println("Dommage ... Mais tu peux réessayer quand tu veux !");}
        println("appuyez sur Entrée pour continuer");
    }

    void relier(Joueur joueur){
        String links="";
        drawASCII("ressources/Chest/game.txt");
        boolean [] abc=new boolean[] {false,false,false};
        String ask="";
        do{
            ask=readString();
            if(charAt(ask,0)=='A'){
                if(length(links)>0){
                    if(abc[0]){
                        if(abc[1] || abc[2]){
                            links=ask+substring(links,2,length(links));
                        }else{
                            links=ask;
                        }
                    }else{
                        links=ask+links;
                        abc[0]=true;
                    }
                }else{
                    links=ask;
                    abc[0]=true;//ajout de la ligne
                }
            }else if(charAt(ask,0)=='B'){
                if(length(links)>0){
                    if(abc[1]){
                        if(abc[0] && abc[2]){
                            links=substring(links,0,2)+ask+substring(links,4,6);
                        }else if(abc[0]){
                            links=substring(links,0,2)+ask;
                        }else if(abc[2]){
                            links=ask+substring(links,2,4);
                        }
                    }else{
                        if(abc[0] && abc[2]){
                            links=substring(links,0,2)+ask+substring(links,2,4);
                        }else if(abc[0]){
                            links=links+ask;
                        }else if(abc[1]){
                            links=ask+links;
                        }
                        abc[1]=true;
                    }
                }else{
                    links=ask;
                    abc[1]=true;
                }
            }else if(charAt(ask,0)=='C'){
                if(length(links)>0){
                    if(abc[2]){
                        links=substring(links,0,length(links)-2);
                    }else{
                        links=links+ask;
                        abc[2]=true;
                    }
                }else{
                    links=ask;
                    abc[2]=true;
                }
            }
            clearTheScreen();
            drawASCII("ressources/Chest/game"+links+".txt");
            println("tapez \"retour\" pour retenter plus tard.");
        }while( !equals(ask,"retour") && !equals(links,"A3B1C2")); // || remplacé par &&
        if(equals(ask,"retour")){
            println("à bientôt !");
        }else{
            clearTheScreen();
            drawASCII("ressources/draw/dessin3.txt");
            println("Tu as trouvé une clé dans le coffre !");
            println("Elle permet peut-etre d'ouvrir la porte de la bibliothèque?");
            joueur.inteligence+=1;
            joueur.inventaire[CLE]=true;
        }
    }
}