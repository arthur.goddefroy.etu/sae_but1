DROP TABLE IF EXISTS Admis;
DROP TABLE IF EXISTS Candidats;
DROP TABLE IF EXISTS Formations;
DROP TABLE IF EXISTS Etablissements;
DROP TABLE IF EXISTS Communes;
DROP TABLE IF EXISTS import;

--\! curl "https://data.enseignementsup-recherche.gouv.fr/api/explore/v2.1/catalog/datasets/fr-esr-parcoursup/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B" > fr-esr-parcoursup.csv;

CREATE TABLE import (
n1 int,
n2 text,
n3 char(8),
n4 text,
n5 char(3),
n6 text,
n7 text,
n8 text,
n9 text,
n10 text,
n11 text,
n12 text,
n13 text,
n14 text,
n15 text,
n16 text,
n17 text,
n18 int,
n19 int,
n20 int,
n21 int,
n22 text,   --laissée en texte car valeur que je n'utilise pas et est souvent vide ou avec des espaces ce que rend impossible l'int
n23 text,   --cf n22
n24 int,
n25 int,
n26 int,
n27 int,
n28 int,
n29 int,
n30 int,
n31 int,
n32 int,
n33 int,
n34 int,
n35 int,
n36 int,
n37 text,   --cf n22
n38 text,   --cf n22
n39 int,
n40 int,
n41 int,
n42 int,
n43 int,
n44 int,
n45 int,
n46 int,
n47 int,
n48 int,
n49 int,
n50 int,
n51 float,
n52 float,
n53 float,
n54 text,  --cf n22
n55 int,
n56 int,
n57 int,
n58 int,
n59 int,
n60 int,
n61 int,
n62 int,
n63 int,
n64 int,
n65 int,
n66 float,
n67 int,
n68 int,
n69 int,
n70 text,   --cf n22
n71 text,   --cf n22
n72 int,
n73 int,
n74 float,
n75 float,
n76 float,
n77 float,
n78 float,
n79 float,
n80 float,
n81 float,
n82 float,
n83 float,
n84 float,
n85 float,
n86 float,
n87 float,
n88 float,
n89 float,
n90 float,
n91 float,
n92 float,
n93 float,
n94 float,
n95 float,
n96 float,
n97 float,
n98 float,
n99 float,
n100 float,
n101 float,
n102 text,
n103 text,   --cf n22
n104 text,
n105 text,   --cf n22
n106 text,
n107 text,   --cf n22
n108 text,
n109 text,
n110 int,
n111 text,
n112 text,
n113 text,   --cf n22
n114 float,
n115 float,
n116 float,
n117 text,
n118 text
);

\copy import FROM fr-esr-parcoursup.csv DELIMITER ';' HEADER;

CREATE TABLE Communes AS 
SELECT DISTINCT n5 AS codeDep,
                n6 AS dep, 
                n7 AS region,
                n9 AS commune
FROM import;

ALTER TABLE Communes ADD PRIMARY KEY(commune, codeDep); -- codeDep est aussi dans la clé primaire car il existe des commune de mêmes nom dans des departements différents.

CREATE TABLE Etablissements AS
SELECT DISTINCT n3 AS IdEta,
                n4 AS etablissement,  
                n8 AS academie,                
                n9 AS communeEta,
                n5 AS codeDep
FROM import;

ALTER TABLE Etablissements ADD PRIMARY KEY(IdEta,communeEta); -- communeEta à été rajouté car certains etablissements imposant se situe sur 2 communes à la fois.
ALTER TABLE Etablissements ADD CONSTRAINT fk_etablissements FOREIGN KEY (communeEta, codeDep) REFERENCES Communes(commune, codeDep);

CREATE TABLE Formations AS
SELECT DISTINCT n110 AS IdForm,
                n3  AS IdEta,
                n11 AS Selectivite,
                n12 AS typeForma,
                n14 AS nomForma,
                n15 AS descForma,
                n16 AS specialité,
                n118 AS lien,
                n9 AS communeEta
FROM import;

ALTER TABLE Formations ADD PRIMARY KEY(IdForm);
ALTER TABLE Formations ADD CONSTRAINT fk_formation FOREIGN KEY (IdEta,communeEta) REFERENCES Etablissements(IdEta,communeEta);

CREATE TABLE Candidats AS 
SELECT DISTINCT n110 AS IdForm,
                n19 AS totalCandi,
                n20 AS candidates,
                n21 AS phase1,           --phase1 = phase principale
                n23 AS phase1NBG,        --NBG = neo bacheliers generaux
                n24 AS phase1NBGbours, 
                n25 AS phase1NBT,        --NBT = neo bacheliers technologique
                n26 AS phase1NBTbours,
                n27 AS phase1NBP,        --NBP = neo bachelier Professionnels
                n28 AS phase1NBPbours,
                n30 AS phase2,           --phase2 = phase complementaire
                n23 AS phase2NBG,       
                n25 AS phase2NBT,        
                n27 AS phase2NBP            
FROM import;

ALTER TABLE Candidats ADD PRIMARY KEY(IdForm);
ALTER TABLE Candidats ADD CONSTRAINT fk_Candidats FOREIGN KEY(IdForm) REFERENCES Formations(IdForm);

CREATE TABLE Admis AS
SELECT DISTINCT n110 AS IdForm,
                n47 AS totalAdmis,
                n48 AS admises,
                n49 AS adPhase1,
                n50 AS adPhase2,
                n57 AS adNBG,
                n58 AS adNBT,
                n59 AS adNBP,
                n60 AS adAutre,
                n62 AS sansMention,
                n63 AS assezBien,
                n64 AS bien,
                n65 AS tresBien,
                n66 AS tresBienFelicite
FROM import;

ALTER TABLE Admis ADD PRIMARY KEY(IdForm);
ALTER TABLE Admis ADD CONSTRAINT fk_Admis FOREIGN KEY(IdForm) REFERENCES Formations(IdForm);
