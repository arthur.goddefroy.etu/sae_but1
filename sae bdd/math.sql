Toutes les formations

SELECT sum(Candidats.totalCandi),sum(Candidats.candidates),Communes.region
FROM Candidats, Formations,Communes
WHERE Candidats.IdForm = Formations.IdForm
AND Formations.communeEta=Communes.commune
GROUP BY Communes.region
HAVING Communes.region='Hauts-de-France';

Formations scientifiques.

SELECT sum(Candidats.totalCandi),sum(Candidats.candidates),Communes.region
FROM Candidats, Formations,Communes
WHERE Candidats.IdForm = Formations.IdForm
AND Formations.communeEta=Communes.commune
AND (Formations.nomForma LIKE '%ingénieur%'
OR Formations.nomForma LIKE '%scient%'
OR Formations.nomForma LIKE '%science%'
OR Formations.nomForma LIKE '%BTS%'
OR Formations.nomForma LIKE '%vét%'
OR Formations.nomForma LIKE '%sanitaire%'
OR Formations.nomForma LIKE '%médecine%')
GROUP BY Communes.region
HAVING Communes.region='Hauts-de-France';

Formations de médecines

SELECT sum(Candidats.totalCandi),sum(Candidats.candidates),Communes.region
FROM Candidats, Formations,Communes
WHERE Candidats.IdForm = Formations.IdForm
AND Formations.communeEta=Communes.commune
AND (Formations.nomForma LIKE '%vét%'
OR Formations.nomForma LIKE '%sanitaire%'
OR Formations.nomForma LIKE '%médecine%')
GROUP BY Communes.region
HAVING Communes.region='Hauts-de-France';

--Toutes formations
--Candidat(e)s|candidates|
--totals   |          | région
-----------+----------+-----------------
-- 1014181 |  567326  | Hauts-de-France
-- Formations scientifiques
--  335521 |  138627  | Hauts-de-France 
-- Formations sanitaires
--  153632 |  122119  | Hauts-de-France
-----------+----------+-----------------
--sanitaires + scientifiques
--  489153 |  260746  | Hauts-de-France

Toutes les formations :
SELECT Formations.Selectivite, sum(Admis.adNBG) AS generaux , sum(Admis.adNBT) AS technologiques, sum(Admis.totalAdmis)
FROM Admis,Formations,Communes
WHERE Admis.IdForm = Formations.IdForm
AND Formations.communeEta=Communes.commune
GROUP BY Communes.region, Formations.Selectivite
HAVING Communes.region='Hauts-de-France'
AND Formations.Selectivite='formation sélective';

Formations scientifiques
SELECT Formations.Selectivite, sum(Admis.adNBG) AS generaux , sum(Admis.adNBT) AS technologiques, sum(Admis.totalAdmis)
FROM Admis,Formations,Communes
WHERE Admis.IdForm = Formations.IdForm
AND Formations.communeEta=Communes.commune
AND (Formations.nomForma LIKE '%ingénieur%' --
OR Formations.nomForma LIKE '%science%'     --
OR Formations.nomForma LIKE '%BTS%')
GROUP BY Communes.region, Formations.Selectivite
HAVING Communes.region='Hauts-de-France'
AND Formations.Selectivite='formation sélective';



--Formations scientifiques
--        selectivite  | generaux | technologiques |  sum  
-----------------------+----------+----------------+-------
-- formation sélective |     4121 |           3365 | 15121
--Toutes les Formations
-----------------------+----------+----------------+-------
-- formation sélective |    15894 |           7915 | 39439



SELECT Formations.typeForma
FROM Formations,Communes
WHERE Formations.communeEta=Communes.commune
GROUP BY Communes.region, Formations.typeForma
HAVING Communes.region='Hauts-de-France';

--     typeforma      |  durée
----------------------+-----------
-- Autre formation    | variable
-- Ecole d'Ingénieur | 3 à 5 ans
-- PASS               | 1 an
-- Licence_Las        | 3 ans
-- EFTS               | 1 an
-- BUT                | 3 ans
-- Ecole de Commerce  | 3 à 5 ans
-- CPGE               | 2 ans
-- Licence            | 3 ans
-- BTS                | 2 ans
-- IFSI               | 3 ans
----------------------+------------
-- Ici je considererais comme études courtes les formation de 2 ans ou moins.

update Candidats
SET phase1NBG='0'
where ((NOT phase2NBG like '%1%')AND
(NOT phase2NBG like '%2%')AND
(NOT phase2NBG like '%3%')AND
(NOT phase2NBG like '%4%')AND
(NOT phase2NBG like '%5%')AND
(NOT phase2NBG like '%6%')AND
(NOT phase2NBG like '%7%')AND
(NOT phase2NBG like '%8%')AND
(NOT phase2NBG like '%9%'));

sum(Cast(phase1NBG AS INTEGER)) AS GeneSans, 
sum(phase1NBT) As TechSans,
sum(phase1NBP) AS ProfSans,

SELECT Formations.typeForma,sum(phase1) AS total,

sum(phase1NBGbours) AS GeneAvec, 
sum(phase1NBTbours) AS TechAvec, 
sum(phase1NBPbours) AS ProfAvec,
(sum(phase1NBPbours) +sum(phase1NBTbours)+sum(phase1NBGbours)) AS totalBours
FROM Formations,Communes,Candidats
WHERE Formations.communeEta=Communes.commune
AND Formations.IdForm = Candidats.IdForm
GROUP BY Communes.region, Formations.typeForma
HAVING Communes.region='Hauts-de-France'
Order By sum(phase1NBPbours) +sum(phase1NBTbours)+sum(phase1NBGbours) DESC;


--     typeforma      | total  | geneavec | techavec | profavec | totalbours
----------------------+--------+----------+----------+----------+------------
-- BTS                | 214902 |     7367 |    15514 |    15446 |      38327
-- Licence            | 217163 |    28240 |     5565 |     1654 |      35459
-- IFSI               | 141979 |     6473 |     8669 |     6822 |      21964
-- BUT                |  74411 |     6539 |     7323 |      681 |      14543
-- PASS               |  80021 |    10835 |     1139 |      317 |      12291
-- Autre formation    |  56639 |     4066 |     2160 |      737 |       6963
-- Licence_Las        |  35029 |     5249 |     1019 |      245 |       6513
-- CPGE               |  57176 |     4767 |      337 |       15 |       5119
-- Ecole d'Ingénieur |  74297 |     4862 |      210 |        0 |       5072
-- Ecole de Commerce  |  22306 |      613 |       42 |       12 |        667
-- EFTS               |   2793 |       92 |      138 |       76 |        306
------------Total nb boursiers :79 103    :42 116    :26 005    :147 224

--PARTIE 2--


SELECT Etablissements.etablissement , Formations.nomForma,Formations.descForma,sum(Admis.totalAdmis)
FROM Admis,Candidats,Formations,Communes,Etablissements
WHERE Admis.IdForm = Formations.IdForm
AND Candidats.IdForm=Formations.IdForm
AND Formations.communeEta=Communes.commune
AND Formations.IdEta=Etablissements.IdEta
AND Formations.typeForma like '%BUT%'
AND Formations.descForma like '%info%'
GROUP BY Communes.region,Etablissements.etablissement,Formations.nomForma,Formations.descForma
HAVING Communes.region='Hauts-de-France'
ORDER BY sum(Admis.totalAdmis) DESC;

--A l'aide de cette requête je récupere tout les IUT de ma région (les hauts de frances) ayant un BUT informatique et choisis les deux avec le plus d'admis pour obtenir des statistiques plus fiables

--                             etablissement                             |     nomforma     |                    descforma                    | sum
-------------------------------------------------------------------------+------------------+-------------------------------------------------+-----
-- Institut universitaire de technologie de Lille - Université de Lille | BUT - Production | Génie électrique et informatique industrielle |  79
-- I.U.T. de Valenciennes                                                | BUT - Production | Génie électrique et informatique industrielle |  43
-- IUT de l'Aisne - Site de Soissons-Cuffies                             | BUT - Production | Génie électrique et informatique industrielle |  40
-- I.U.T. de Béthune                                                    | BUT - Production | Génie électrique et informatique industrielle |  39
-- I.U.T LITTORAL (CALAIS)                                               | BUT - Production | Génie électrique et informatique industrielle |  28

--Je travaillerais donc sur l'IUT de Lille et L'IUT de Valenciennes.

SELECT sum(Admis.totalAdmis),Communes.region, Etablissements.etablissement
FROM Admis, Formations,Communes,Etablissements
WHERE Admis.IdForm = Formations.IdForm
AND Formations.communeEta=Communes.commune
AND Formations.IdEta=Etablissements.IdEta
AND (
      Etablissements.etablissement LIKE  '%Institut universitaire de technologie%'
      OR Etablissements.etablissement LIKE '%IUT%'
      OR Etablissements.etablissement LIKE '%I.U.T'
)
GROUP BY Communes.region,Etablissements.etablissement
HAVING Communes.region='Hauts-de-France'
ORDER BY sum(Admis.totalAdmis) DESC;


-- Je travaillerais donc avec l'IUT d'amien et L'IUT de Lille

-- comparaison nb filles

SELECT Communes.region, Etablissements.etablissement,sum(Admis.totalAdmis) AS nb_admis,sum(Candidats.totalCandi) AS nb_Candidats,sum(Admis.admises) AS nb_filles_admises,sum(Candidats.candidates) AS nb_filles_candidats
FROM Admis,Candidats,Formations,Communes,Etablissements
WHERE Admis.IdForm = Formations.IdForm
AND Formations.communeEta=Communes.commune
AND Formations.IdEta=Etablissements.IdEta
AND Candidats.IdForm=Formations.IdForm
AND (
      Etablissements.etablissement ='Institut universitaire de technologie de Lille - Université de Lille'
      OR Etablissements.etablissement = 'I.U.T. de Valenciennes'
)
GROUP BY Communes.region,Etablissements.etablissement
HAVING Communes.region='Hauts-de-France'
ORDER BY sum(Admis.totalAdmis) DESC;

--     region      |                             etablissement                             | nb_admis | nb_candidats | nb_filles_admises | nb_filles_candidats
-------------------+-----------------------------------------------------------------------+----------+--------------+-------------------+---------------------
-- Hauts-de-France | Institut universitaire de technologie de Lille - Université de Lille  |      717 |        13383 |               249 |                5669
-- Hauts-de-France | I.U.T. de Valenciennes                                                |      464 |         5562 |               169 |                2079

--comparaison mention au bacs (faire un total des mentions puis les mention une par une)

SELECT Communes.region, Etablissements.etablissement, sum(Admis.assezBien) AS assezBien, sum(Admis.bien) AS bien, sum(Admis.tresBien) AS tresBien, sum(Admis.tresBienFelicite) AS Felicitation, sum(Admis.totalAdmis) AS totalAdmis
FROM Admis,Candidats,Formations,Communes,Etablissements
WHERE Admis.IdForm = Formations.IdForm
AND Formations.communeEta=Communes.commune
AND Formations.IdEta=Etablissements.IdEta
AND Candidats.IdForm=Formations.IdForm
AND (
      Etablissements.etablissement ='Institut universitaire de technologie de Lille - Université de Lille'
      OR Etablissements.etablissement = 'I.U.T. de Valenciennes'
)
GROUP BY Communes.region,Etablissements.etablissement
HAVING Communes.region='Hauts-de-France'
ORDER BY sum(Admis.totalAdmis) DESC;

--     region      |                            etablissement                             | assezbien | bien | tresbien | felicitation | totaladmis 
-------------------+----------------------------------------------------------------------+-----------+------+----------+--------------+------------
-- Hauts-de-France | Institut universitaire de technologie de Lille - Université de Lille |       230 |  176 |       35 |            2 |        717
-- Hauts-de-France | I.U.T. de Valenciennes                                               |       149 |   67 |       10 |            0 |        464



--comparaison taux de generaux, technologiques et proffessionels

update Candidats
SET phase2NBG='0'
where ((NOT phase2NBG like '%1%')AND
(NOT phase2NBG like '%2%')AND
(NOT phase2NBG like '%3%')AND
(NOT phase2NBG like '%4%')AND
(NOT phase2NBG like '%5%')AND
(NOT phase2NBG like '%6%')AND
(NOT phase2NBG like '%7%')AND
(NOT phase2NBG like '%8%')AND
(NOT phase2NBG like '%9%'));

SELECT Communes.region, Etablissements.etablissement,
sum(Admis.adNBG),
sum(Cast(phase1NBG AS INTEGER))+sum(Cast(phase2NBG AS INTEGER)),
sum(Admis.adNBT),
sum(Candidats.phase1NBT)+sum(Candidats.phase2NBT),
sum(Admis.adNBP),
sum(Candidats.phase1NBP)+sum(Candidats.phase2NBP),
sum(Admis.adAutre),
sum(Candidats.totalCandi)-(sum(Candidats.phase1NBP)+sum(Candidats.phase2NBP)+sum(Cast(phase1NBG AS INTEGER))+sum(Cast(phase2NBG AS INTEGER))+sum(Candidats.phase1NBT)+sum(Candidats.phase2NBT))
FROM Admis,Candidats,Formations,Communes,Etablissements
WHERE Admis.IdForm = Formations.IdForm
AND Formations.communeEta=Communes.commune
AND Formations.IdEta=Etablissements.IdEta
AND Candidats.IdForm=Formations.IdForm
AND (
      Etablissements.etablissement ='Institut universitaire de technologie de Lille - Université de Lille'
      OR Etablissements.etablissement = 'I.U.T. de Valenciennes'
)
GROUP BY Communes.region,Etablissements.etablissement
HAVING Communes.region='Hauts-de-France'
ORDER BY sum(Admis.totalAdmis) DESC;